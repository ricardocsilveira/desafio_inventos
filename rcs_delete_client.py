from _helper import get_connection
from settings import DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME


def lambda_handler(event, context):
    con = get_connection(host=DB_HOST,
                         port=DB_PORT,
                         user=DB_USER,
                         password=DB_PASSWORD,
                         name=DB_NAME)
    if not con:
        return {"error": "invalid arguments to connect!"}
    try:
        c = con.cursor()
        sql = "DELETE FROM client WHERE client_id = %d" % int(client_id)
        c.execute(sql)
        deleted_rows = 0
        deleted_rows = c.rowcount
        con.commit()
        con.close()
        return deleted_rows
    except Exception, e_msg:
        return {"error": str(e_msg)}
    finally:
        if con is not None:
            con.close()
