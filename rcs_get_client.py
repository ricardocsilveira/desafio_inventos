from _helper import get_connection
from settings import DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME


def lambda_handler(event, context):
    con = get_connection(host=DB_HOST,
                         port=DB_PORT,
                         user=DB_USER,
                         password=DB_PASSWORD,
                         name=DB_NAME)
    if not con:
        return {"error": "invalid arguments to connect!"}
    try:
        c = con.cursor()
        client_id = event.get("id", None)
        sql = "SELECT client_id, client_name, client_email, client_cpf FROM client"
        if client_id:
            sql = "%s %s" % (sql, "WHERE client_id = %d" % int(client_id))
        c.execute(sql)
        results = []
        for row in c.fetchall():
            row_dict = {}
            row_dict["id"] = row[0]
            row_dict["name"] = row[1].rstrip()
            row_dict["email"] = row[2].rstrip()
            row_dict["cpf"] = row[3].rstrip()
            results.append(row_dict)
        c.close()
        return {"data": results}
    except Exception, e_msg:
        return {"error": str(e_msg)}
    finally:
        if con is not None:
            con.close()
