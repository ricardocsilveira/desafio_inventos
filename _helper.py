import psycopg2


def get_connection(**kwargs):
    """
    Returns connection instance to remote database
    """
    db_host = kwargs.get("host", None)
    db_port = kwargs.get("port", None)
    db_user = kwargs.get("user", None)
    db_password = kwargs.get("password", None)
    db_name = kwargs.get("name", None)
    if not(db_host and db_port and db_user and db_password and db_name):
        return False
    try:
        return psycopg2.connect(host=db_host,
                                port=db_port,
                                user=db_user,
                                password=db_password,
                                dbname=db_name)
    except:
        # what is the correct error to be handled?
        return {"error": "failed to connect to database!"}
