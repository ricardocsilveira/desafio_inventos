from _helper import get_connection
from settings import DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME


def lambda_handler(event, context):
    con = get_connection(host=DB_HOST,
                         port=DB_PORT,
                         user=DB_USER,
                         password=DB_PASSWORD,
                         name=DB_NAME)
    if not con:
        return {"error": "invalid arguments to connect!"}
    c = con.cursor()
    fields_list = ["name", "cpf", "email"]
    try:
        client_id = event["id"]
        sql = "UPDATE client SET"
        changed_sql = False
        for field in fields_list:
            field_value = event.get(field, None)
            if field_value:
                sql = "%s client_%s = '%s'," % (sql, field, field_value)
                changed_sql = True
        if not changed_sql:
            return {"error": "no values to update"}
        sql = "%s WHERE client_id = %d" % (sql.rstrip(","), client_id)
        c.execute(sql)
        con.commit()
        c.close()
        return True
    except KeyError:
        return {"error": "missing id"}
    except Exception, e_msg:
        return {"error": str(e_msg)}
    finally:
        if con is not None:
            con.close()
