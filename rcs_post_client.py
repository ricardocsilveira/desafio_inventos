from _helper import get_connection
from settings import DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME


def lambda_handler(event, context):
    try:
        client_name = event["name"]
        client_cpf = event["cpf"]
        client_email = event["email"]
    except KeyError:
        return {"error": "missing fields!"}
    con = get_connection(host=DB_HOST,
                         port=DB_PORT,
                         user=DB_USER,
                         password=DB_PASSWORD,
                         name=DB_NAME)
    if not con:
        return {"error": "invalid arguments to connect!"}
    try:
        c = con.cursor()
        sql = "INSERT INTO client (client_name, client_email, client_cpf)  VALUES ('%s','%s','%s')" % (client_name, client_email, client_cpf)
        c.execute(sql)
        con.commit()
        con.close()
        return True
    except Exception, e_msg:
        return {"error": str(e_msg)}
    finally:
        if con is not None:
            con.close()
